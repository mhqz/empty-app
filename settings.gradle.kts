pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()

        //maven {
        //    url "https://s01.oss.sonatype.org/content/repositories/ieequalitouinet-1234"
        //    content {
        //        // this repository *only* contains artifacts with group "ie.equalit.ouinet"
        //        includeGroup "ie.equalit.ouinet"
        //    }
        //}

        flatDir {
            dirs("app/libs")
        }
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "EmptyApp"
include(":app")
